%% init...
clear all global
restoredefaultpath
o_ptb.init_ptb();

ptb_cfg = o_ptb.PTB_Config();
ptb_cfg.psychportaudio_config.reqlatencyclass = 0;
ptb_cfg.psychportaudio_config.simulate_audio_cutoff = true;
ptb_cfg.audio_system_cutoff = 3000;

ptb = o_ptb.PTB.get_instance(ptb_cfg);

ptb.setup_audio();

%% load sound files
alice = o_ptb.stimuli.auditory.Wav(fullfile('snippets', 'assets', 'alice.mp3'));
huckleberry = o_ptb.stimuli.auditory.Wav(fullfile('snippets', 'assets', 'huckleberry_finn.mp3'));

huckleberry.lufs = alice.lufs;

%% prepare playing them
ptb.prepare_audio(alice);
ptb.prepare_audio(huckleberry, 3, true, true);

%% play
ptb.schedule_audio();
ptb.play_without_flip();